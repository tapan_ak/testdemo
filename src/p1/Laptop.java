package p1;
class Laptop {
	private int SerialNumber;
	private String BrandName;
	private int Cost;
	
	public int getSerialNumber() {
		return SerialNumber;
	}

	public void setSerialNumber(int SerialNumber) {
		SerialNumber = SerialNumber;
	}

	public String getBrandName() {
		return BrandName;
	}

	public void setBrandName(String BrandName) {
		BrandName = BrandName;
	}

	public int getCost() {
		return Cost;
	}

	public void setCost(int Cost) {
		Cost = Cost;
	}

	Laptop() {
	}// default constructor

	Laptop(int SerialNumber, String BrandName, int Cost) {
		this.SerialNumber = SerialNumber;
		this.BrandName = BrandName;
		this.Cost = Cost;
	}

	@Override
	public String toString() {
		return "Laptop [SerialNumber=" + SerialNumber + ", BrandName=" + BrandName + ", Cost=" + Cost + "]";
	}

}
