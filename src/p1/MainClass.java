package p1;

import java.util.Scanner;

public class MainClass {

	static Scanner sc = new Scanner(System.in);
	static MainClass run = new MainClass();
	LaptopRecords lap = new LaptopRecords();
	static int SerialNumber, Cost;

	public static void main(String[] args) {

		
		while (true) {
			System.out.println("Add Laptop Details:1");
			System.out.println("Search by serial:2");
			System.out.println("Search by max cost:3");
			System.out.println("Exit:4");
			int a = Integer.parseInt(sc.nextLine());
			
			switch (a) {
			case 1:
				System.out.println("Add Laptop Details:");
				run.addLaptop();
				break;
			case 2:
				System.out.println("Search by SerialNumber:");
				run.searchSerialNumber();
				break;
			case 3:
				System.out.println("Search by Max Cost:");
				run.searchMaxCost();
				break;
			case 4:
				System.exit(0);
			default:
				System.out.println("Wrong Choice");
			}
		}

	}

	public void addLaptop() {

		System.out.println("Enter Serial Number ");
		int SerialNumber = Integer.parseInt(sc.nextLine());

		System.out.println("Enter Brand Name ");
		String BrandName = sc.nextLine();

		System.out.println("Enter Cost ");
		int Cost = Integer.parseInt(sc.nextLine());

		Laptop l = new Laptop(SerialNumber, BrandName, Cost);

		lap.addLaptop(l);
	}

	private void searchMaxCost() {
		System.out.println("Enter max cost");
		int Cost = Integer.parseInt(sc.nextLine());
		lap.searchByMaxCost(Cost);
	}

	private void searchSerialNumber() {
		int SerialNumber = Integer.parseInt(sc.nextLine());
		lap.searchBySerialNumber(SerialNumber);
	}

}
