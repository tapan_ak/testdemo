//LaptopRecord
package p1;

import java.util.Scanner;
//adding a comment for reflecting some change

class LaptopRecords {
	Scanner sc = new Scanner(System.in);
	// LaptopRecords lap1 = new LaptopRecords();
	Laptop lptp[] = new Laptop[100];
	int counter = 0;

	public void addLaptop(Laptop l) {
		lptp[counter++] = l;
	}

	public void printLaptopRecords() {
		for (int i = 0; i < counter; i++) {
			System.out.println("SerialNumber : " + lptp[i].getSerialNumber());
			System.out.println("Laptop Brand : " + lptp[i].getBrandName());
			System.out.println("Cost : " + lptp[i].getCost());
		}
	}

	void searchByMaxCost(int cost) {
		for (int i = 0; i < lptp.length; i++) {

			if (cost < lptp[i].getCost()) {
				printLaptopRecords();

			}

		}
	}

	public void searchBySerialNumber(int serialNumber) {
		printLaptopRecords();
	}

}
